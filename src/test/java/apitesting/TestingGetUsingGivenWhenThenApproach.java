package apitesting;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.response.ValidatableResponse;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class TestingGetUsingGivenWhenThenApproach {
  @Test
  public void testStatusCode() {
	  given().get("https://reqres.in/api/users?page=2").then().statusCode(200);
  }
  
  @Test
  public void testParticularValue() {
	  baseURI="https://reqres.in";
	  given().get("/api/users?page=2").then().body("data[1].email", equalTo("lindsay.ferguson@reqres.in"));
  }
  
  @Test
  public void printAllValues() {
	  given().get("https://reqres.in/api/users?page=2").then().log().all();
  }
  
  
}
