package apitesting;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.*;

public class TestingGetSingleUser {
  @Test
  public void GetLastName() {
	  
	  given().get("https://reqres.in/api/users/2").then().body("data.last_name", equalTo("Weaver"));
  }
}
