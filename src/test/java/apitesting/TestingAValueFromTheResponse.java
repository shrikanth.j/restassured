package apitesting;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

public class TestingAValueFromTheResponse {
  @Test
  public void TestingAValue() {
	   baseURI = "https://reqres.in";
	  given().get("/api/users?page=2").then().body("data.first_name", hasItem("Lindsay"));
  }
  
  @Test
  public void TestingMultipleValue() {
	   baseURI = "https://reqres.in";
	  given().get("/api/users?page=2").then().body("data.first_name", hasItems("Lindsay", "Michael", "George"));
 }
  
  @Test
  public void TestingMultipleValueWithMultipleBody() {
	   baseURI = "https://reqres.in";
	  given().get("/api/users?page=2").then().body("data.first_name", hasItems("Lindsay", "Michael", "George"))
	  .body("data.email", hasItem("george.edwards@reqres.in"));
 }
  
}
