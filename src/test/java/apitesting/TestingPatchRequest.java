package apitesting;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class TestingPatchRequest {
  @Test
  public void updatingDataUsingPatch() {
	  JSONObject request = new JSONObject();
	  request.put("name", "Jayanna");
	  request.put("job", "Farmer");
	  
	  System.out.println(request);
	  
	  given().body(request.toJSONString()).patch("https://reqres.in/api/users/2").then().statusCode(200)
	  .body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:59:28.057Z"));
  }
}
