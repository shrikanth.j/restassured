package apitesting;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;

public class PostOperation {
  @Test
  public void postOperationTest() {
	  JSONObject request = new JSONObject();
	  request.put("name", "Jayanna");
	  request.put("job", "Farmer");
	  
	  System.out.println(request);
	  
	  baseURI="https://reqres.in/api";
	  given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
  }
}
