package apitesting;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matcher.*;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;



public class TestingPutRequest {
  @Test
  public void updatingDataUsingPut() {
	  JSONObject request = new JSONObject();
	  request.put("name", "Jayanna");
	  request.put("job", "Farmer");
	  
	  System.out.println(request);
	  
	  given().body(request.toJSONString()).put("https://reqres.in/api/users/2").then().statusCode(200)
	  .body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:59:28.057Z"));
  }
}
